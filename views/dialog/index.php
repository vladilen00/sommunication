<?php
/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $model \app\modules\communication\models\Sender */
/* @var $dialogId integer */

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use app\widgets\ActiveForm;

$this->title = Yii::t('app/communication', 'views.message.search.text-2') . ' | ' . Yii::$app->name;
$this->params['breadcrumbs'][] = Yii::t('app/communication', 'views.message.search.text-2');
$this->params['search'] = [
	'action' => ['message/search'],
	'options' => [
		'id' => 'comm-message-search-form',
	],
	'inputOptions' => [
		'placeholder' => Yii::t('app/communication', 'views.message.search.text-1')
	],
];

$this->registerAssetBundle('app\modules\communication\assets\DialogAsset');
?>
<div class="card clearfix" id="messages" style="min-height: 350px;">
	<div class="ms-menu p-b-0 c-overflow">
		<div class="action-header bg palette-Grey-100">
			<div class="ah-label"><?php echo Yii::t('app/communication', 'views.dialog.index.text-1') ?></div>
		</div>
		<?
		Pjax::begin([
			'id' => 'comm-dialog-container',
			'linkSelector' => '#comm-dialog-list .pagination a',
			'enablePushState' => false,
		]);
		echo ListView::widget([
			'id' => 'comm-dialog-list',
			'dataProvider' => $dataProvider,
			'layout' => '{items}{pager}',
			'options' => ['class' => 'list-group lg-alt m-t-10'],
			'itemOptions' => ['tag' => false],
			'itemView' => 'partial/index-item',
			'beforeItem' => function ($model, $key, $index) use ($dialogId) {
				if ($index === 0 && !empty($dialogId) && !Yii::$app->request->isAjax) {
					return Html::a('', ['enter', 'id' => $dialogId], ['class' => 'comm-dialog-list-item initial hidden']);
				}
			},
			'pager' => [
				'firstPageLabel' => false,
				'lastPageLabel' => false,
				'maxButtonCount' => 0
			],
			'emptyText' => Yii::t('app/communication', 'views.dialog.index.text-2'),
			'emptyTextOptions' => ['class' => 'list-group-item text-center']
		]);
		Pjax::end();
		?>
	</div>
	<div class="ms-body" id="comm-message-container">
		<div class="action-header clearfix bg palette-Grey-100">
			<div class="ah-label">
				<div class="menu-collapse visible-xs" data-ma-action="message-toggle">
					<div class="mc-wrap">
						<div class="mcw-line top bgm-gray"></div>
						<div class="mcw-line center bgm-gray"></div>
						<div class="mcw-line bottom bgm-gray"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="list-group lg-alt">
			<div class="list-group-item text-center">
				<i class="zmdi zmdi-comment-list zmdi-hc-5x m-b-20"></i><br/>
				<?php echo Yii::t('app/communication', 'views.dialog.index.text-3') ?><br/>
				<?php echo Yii::t('app/communication', 'views.dialog.index.text-4')?>
			</div>
		</div>
	</div>
</div>
