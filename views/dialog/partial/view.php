<?php
/* @var $this \yii\web\View */
/* @var $dialog \app\modules\communication\models\Dialog */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use app\modules\user\widgets\UserImg;

/* @var $model \app\modules\communication\models\Message */
?>
<? foreach ($dataProvider->models as $model): ?>
	<div class="list-group-item comm-message-list-item media<?= $model->isMy ? ' text-right' : ''; ?>" data-key="<?= $model->id; ?>">
		<div class="text-center <?= $model->isMy ? 'pull-right' : 'pull-left'; ?>">
			<?= UserImg::widget(['model' => $model->user, 'link' => true]); ?>
			<div class="ac-check">
				<input type="checkbox" class="comm-message-list-item-input acc-check" value="<?= $model->id; ?>"/>
				<span class="acc-helper"></span>
			</div>
		</div>
		<div class="media-body">
			<div class="msb-item c-pointer bg <?= $model->isMy ? 'palette-Blue-400 c-white' : 'palette-Grey-200'; ?>">
				<?= $model->getText(); ?>
			</div>
			<small class="ms-date"><i class="zmdi zmdi-time"></i> <?= Yii::$app->formatter->asDatetime($model->created_at); ?></small>
		</div>
	</div>
<? endforeach; ?>
