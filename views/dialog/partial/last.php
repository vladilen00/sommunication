<?php
/* @var $this \yii\web\View */
/* @var $models \app\modules\communication\models\Dialog[] */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\user\widgets\UserImg;
?>
<? if (!empty($models)): ?>
	<div class="list-group lg-alt">
		<? foreach ($models as $model): ?>
			<a href="<?= Url::to(['index', 'id' => $model->id]); ?>" class="list-group-item media">
				<div class="pull-left">
					<?=
					UserImg::widget([
						'model' => $model->users[0],
						'imageOptions' => ['class' => 'a-lg']
					]);
					?>
				</div>

				<? if ($model->new_message_count > 0): ?>
					<div class="pull-right">
						<span class="label label-primary m-t-20 d-ib"><?= $model->new_message_count; ?></span>
					</div>
				<? endif; ?>
				<div class="media-body">
					<div class="lgi-heading m-b-0"><?= implode(', ', ArrayHelper::getColumn($model->users, 'profile.name')); ?></div>
					<div class="lgi-text m-b-0">
						<? if ($model->lastMessage->user_id == Yii::$app->user->id): ?>
							<b><?php echo Yii::t('app/communication', 'views.dialog.partial.last.text-1') ?></b>
						<? endif; ?>
						<?= $model->lastMessage->text; ?>
					</div>
					<div class="f-10 text palette-Grey-400"><?= Yii::$app->formatter->asRelativeTime($model->lastMessage->created_at); ?></div>
				</div>
			</a>
		<? endforeach; ?>
	</div>
<? else: ?>
	<div class="p-20 text-center">
		<i class="zmdi zmdi-info-outline zmdi-hc-5x"></i><br/>
		<?php echo Yii::t('app/communication', 'views.dialog.partial.last.text-2') ?>
	</div>
<? endif; ?>

