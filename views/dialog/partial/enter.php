<?php
/* @var $this \yii\web\View */
/* @var $model \app\modules\communication\models\Dialog */
/* @var $sender \app\modules\communication\models\Sender */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Dropdown;
?>
<div class="action-header bg palette-Grey-100">
	<div class="ah-label">
		<div class="menu-collapse visible-xs" data-ma-action="message-toggle">
			<div class="mc-wrap">
				<div class="mcw-line top bgm-gray"></div>
				<div class="mcw-line center bgm-gray"></div>
				<div class="mcw-line bottom bgm-gray"></div>
			</div>
		</div>
		<?= implode(', ', ArrayHelper::getColumn($model->users, 'profile.first_name')); ?>
	</div>

	<?= Html::beginForm(['message/search', 'dialog_id' => $model->id], 'GET', ['id' => 'comm-message-search-form', 'class' => 'ah-search']); ?>
	<input type="text" placeholder="<?php echo Yii::t('app/communication', 'views.dialog.partial.enter.text-1') ?>" class="ahs-input" name="q" autocomplete="off">
	<i class="ah-search-close zmdi zmdi-long-arrow-left" data-ma-action="ah-search-close"></i>
	<?= Html::endForm(); ?>

	<ul class="actions">
		<li>
			<?=
			Html::a('<i class="zmdi zmdi-delete"></i>', ['message/hide'], [
				'id' => 'comm-message-hide',
				'title' => Yii::t('app/communication', 'views.dialog.partial.enter.text-2'),
				'data' => [
					'swal-confirm' => Yii::t('app/communication', 'views.dialog.partial.enter.text-3'),
					'swal-method' => 'false'
				],
				'style' => 'display: none',
			]);
			?>
		</li>
		<li>
			<a href="#" class="ah-search-trigger" data-ma-action="ah-search-open" title="<?php echo Yii::t('app/communication', 'views.dialog.partial.enter.text-4') ?>">
				<i class="zmdi zmdi-search"></i>
			</a>
		</li>
		<li class="dropdown">
			<a href="#" data-toggle="dropdown">
				<i class="zmdi zmdi-more-vert"></i>
			</a>
			<?=
			Dropdown::widget([
				'encodeLabels' => false,
				'options' => ['class' => 'dropdown-menu-right dm-icon'],
				'items' => [
					[
						'label' => '<i class="zmdi zmdi-close"></i> ' .Yii::t('app/communication', 'views.dialog.partial.enter.text-5'),
						'url' => ['hide', 'id' => $model->id],
						'linkOptions' => [
							'data' => [
								'swal-confirm' => Yii::t('app/communication', 'views.dialog.partial.enter.text-6'),
								'swal-method' => 'post'
							]
						]
					],
					[
						'label' => '<i class="zmdi zmdi-account-add"></i> ' . Yii::t('app/communication', 'views.dialog.partial.enter.text-7'),
						'url' => '#',
						'visible' => YII_ENV_DEV
					]
				]
			]);
			?>
		</li>
	</ul>
</div>
<div id="comm-message-list" class="list-group lg-alt" style="margin-bottom: 85px; min-height: 190px;">
	<a href="#" class="list-group-item text-center c-gray" id="comm-message-load-more">
		<i class="zmdi zmdi-long-arrow-up zmdi-hc-fw"></i> <?php echo Yii::t('app/communication', 'views.dialog.partial.enter.text-8') ?>
	</a>
	<?= $content; ?>
</div>
<?
echo Html::beginForm(['send', 'id' => $model->id], 'POST', [
	'id' => 'comm-message-form',
	'class' => 'ms-reply m-t-0',
]);
echo Html::activeTextarea($sender, 'text', ['maxlength' => true, 'placeholder' => Yii::t('app/communication', 'views.dialog.partial.enter.text-9')]);
echo Html::submitButton('<i class="zmdi zmdi-mail-send"></i>');
echo Html::endForm();

