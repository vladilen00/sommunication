<?php
/* @var $this \yii\web\View */
/* @var $model \app\modules\communication\models\Dialog */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\user\widgets\UserImg;
?>
<a class="list-group-item media comm-dialog-list-item<?= $model->new_message_count > 0 ? ' has-new-messages' : ''; ?>" href="<?= Url::to(['enter', 'id' => $model->id]); ?>">
	<div class="pull-left">
		<? if (count($model->users) > 1): ?>
			<div class="avatar-char bg palette-Blue">
				<i class="zmdi zmdi-accounts-alt"></i>
			</div>
		<? else: ?>
			<?= UserImg::widget(['model' => $model->users[0]]); ?>
		<? endif; ?>
	</div>

	<? if ($model->new_message_count > 0): ?>
		<div class="pull-right">
			<span class="label label-primary m-t-15 d-ib"><?= $model->new_message_count; ?></span>
		</div>
	<? endif; ?>
	<div class="media-body">
		<div class="lgi-heading">
			<? if (count($model->users) > 1): ?>
				<?= implode(', ', ArrayHelper::getColumn($model->users, 'profile.first_name')); ?>
			<? else: ?>
				<?= $model->users[0]->profile->name; ?>
			<? endif; ?>
		</div>
		<small class="lgi-text">
			<? if ($model->lastMessage !== null): ?>
				<? if ($model->lastMessage->user_id == Yii::$app->user->id): ?>
					<b><?php echo Yii::t('app/communication', 'views.dialog.partial.last.text-1')?></b>
				<? elseif (count($model->users) > 1): ?>
					<b><?= $model->lastMessage->user->profile->first_name; ?>:</b>
				<? endif; ?>
				<?= $model->lastMessage->text; ?>
			<? else: ?>
				<i class="zmdi zmdi-alert-triangle"></i>
				<small><?php echo Yii::t('app/communication', 'views.dialog.partial.last.text-2')?></small>
			<? endif; ?>
		</small>
	</div>
</a>

