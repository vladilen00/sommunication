<?php
/* @var $this \yii\web\View */
/* @var $model \app\modules\communication\models\Message */

use yii\helpers\Html;
use app\modules\user\widgets\UserImg;
?>
<div class="list-group-item comm-message-list-item media">
	<div class="text-center pull-left">
		<?= UserImg::widget(['model' => $model->user, 'link' => true, 'imageOptions' => ['class' => 'a-lg']]); ?>
		<div class="ac-check">
			<input type="checkbox" class="comm-message-list-item-input acc-check" value="<?= $model->id; ?>"/>
			<span class="acc-helper"></span>
		</div>
	</div>
	<div class="media-body">
		<div class="msb-item bg <?= $model->isMy ? 'palette-Blue-400 c-white' : 'palette-Grey-200'; ?>">
			<p>
				<span class="f-700"><?= $model->user->profile->name; ?></span><br/>
				<small><i class="zmdi zmdi-time"></i> <?= Yii::$app->formatter->asDatetime($model->created_at); ?></small>
			</p>
			<?= $model->getText(); ?>
			<p>
				<a href="#" data-swal-alert="<?php echo Yii::t('app/communication', 'views.message.partial.search-item.text-1') ?>">
					<i class="zmdi zmdi-long-arrow-right zmdi-hc-lg zmdi-hc-fw"></i>
					<?php echo Yii::t('app/communication', 'views.message.partial.search-item.text-2') ?>
				</a>
			</p>
		</div>
	</div>
</div>

