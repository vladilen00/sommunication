<?php
/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = Yii::t('app/communication', 'views.message.search.text-1') . ' | ' . Yii::t('app/communication', 'views.message.search.text-2') . ' | ' . Yii::$app->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/communication', 'views.message.search.text-2'), 'url' => ['dialog/index']];
$this->params['breadcrumbs'][] = Yii::t('app/communication', 'views.message.search.text-3');
$this->params['search'] = [
	'action' => ['search'],
	'options' => [
		'id' => 'comm-message-search-form',
	],
	'inputOptions' => [
		'placeholder' => Yii::t('app/communication', 'views.message.search.text-3')
	],
];
?>
<div class="card">
	<?
	Pjax::begin([
		'formSelector' => '#comm-message-search-form',
		'options' => ['class' => 'pjax-loader pjax-scroll']
	]);
	echo ListView::widget([
		'dataProvider' => $dataProvider,
		'layout' => '<div class="card-header">{summary}</div><div class="card-body">{items}</div>{pager}',
		'summary' => Yii::t('app/communication', 'views.message.search.text-4') . ' <b>{totalCount}</b>',
		'options' => ['class' => 'list-view list-group lg-alt'],
		'itemView' => 'partial/search-item',
		'itemOptions' => ['tag' => false],
		'emptyText' => '<i class="zmdi zmdi-search-for zmdi-hc-5x"></i><br/> ' .Yii::t('app/communication', 'views.message.search.text-5'),
		'emptyTextOptions' => ['class' => 'card-body card-padding text-center'],
		'pager' => [
			'options' => ['class' => 'pagination pagination-centered p-l-30 p-r-30']
		]
	]);
	Pjax::end();
	?>
</div>

