<?php
/* @var $this \yii\web\View */
/* @var $min boolean */
/* @var $model \app\modules\communication\models\Notification $model */

use yii\helpers\Html;
?>

<? if ($min): ?>
	<?= Yii::t('app/notification', 'social-area-assign-text-1', ['area_names' => "<b>{$model->data->areas}</b>"]); ?>
<? else: ?>
	<p class="m-b-10">
		<?= Yii::t('app/notification', 'social-area-assign-text-1', ['area_names' => "<b>{$model->data->areas}</b>"]); ?>
	</p>

	<? if (!empty($model->data->message)): ?>
		<blockquote class="f-13"><?= $model->data->message; ?></blockquote>
	<? endif; ?>

	<?= Html::a('<i class="zmdi zmdi-plus"></i> ' . Yii::t('app/notification', 'social-area-assign-text-2'), ['/user/area/request', '#' => $model->user_id], ['class' => 'view-more', 'data-pjax' => 0]); ?>
<? endif; ?>