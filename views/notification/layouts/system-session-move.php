<?php
/* @var $this \yii\web\View */
/* @var $min boolean */
/* @var $model \app\modules\communication\models\Notification */

use yii\helpers\Html;
?>

<? if ($min): ?>
	<i class="icon fa-clock-o text-warning"></i> 
	<?
	Yii::t('app/notification', 'system-session-move-text-1', [
		'service_name' => '<b>«' . $model->getDataLangAttribute('service_name') . '»</b>',
	]);
	?>
<? else: ?>
	<i class="icon fa-clock-o text-warning"></i> 
	<?= Yii::t('app/notification', 'system-session-move-text-1', [
		'service_name' => Html::a('<b>«' . $model->getDataLangAttribute('service_name') . '»</b>', ['/session/default/index', 'expert_id' => $model->user_id, '#' => "session={$model->data->session_id}"], ['data-pjax' => 0]),
		'old_begin_at' => Yii::$app->formatter->asDatetime($model->data->old_begin_at),
		'old_end_at' => Yii::$app->formatter->asDatetime($model->data->old_end_at),
		'begin_at' => Yii::$app->formatter->asDatetime($model->data->begin_at),
		'end_at' => Yii::$app->formatter->asDatetime($model->data->end_at),
	]); ?>
<? endif; ?>

