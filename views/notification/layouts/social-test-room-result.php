<?php
/* @var $this \yii\web\View */
/* @var $min boolean */
/* @var $model \app\modules\communication\models\Notification */
?>

<? if ($min): ?>
	<?=
	Yii::t('app/notification', 'social-test-room-result-text-1', [
		'test_name' => '«' . $model->getDataLangAttribute('test_name') . '»',
	]);
	?>
<? else: ?>
	<p class="m-b-10">
		<?=
		Yii::t('app/notification', 'social-test-room-result-text-1', [
			'test_name' => '<b>«' . $model->getDataLangAttribute('test_name') . '»</b>',
		]);
		?>
	</p>
	<a href="<?= $model->data->test_result_url; ?>" class="view-more" data-pjax="0">
		<i class="zmdi zmdi-long-arrow-right"></i> <?= Yii::t('app/notification', 'social-test-room-result-text-2'); ?>
	</a>
<? endif; ?>


