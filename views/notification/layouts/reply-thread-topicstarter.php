<?php
/* @var $this \yii\web\View */
/* @var $min boolean */
/* @var $model \app\modules\communication\models\Notification */

use yii\helpers\Html;
?>

<? if ($min): ?>
	<?=
	Yii::t('app/notification', 'reply-thread-topicstarter-text-1', [
		'thread_name' => "«{$model->data->thread_name}»"
	]);
	?>
<? else: ?>
	<p class="m-b-10">
		<?=
		Yii::t('app/notification', 'reply-thread-topicstarter-text-1', [
			'thread_name' => Html::a("«{$model->data->thread_name}»", $model->data->thread_url, ['data-pjax' => 0]),
		]);
		?>
	</p>
	<blockquote class="f-13">
		<small class="m-b-10"><?= $model->user->profile->name; ?>:</small>
		<?= $model->data->text_preview; ?>
		<a href="<?= $model->data->post_url; ?>" data-pjax="0" class="zmdi zmdi-open-in-new m-l-5"></a>
	</blockquote>
<? endif; ?>