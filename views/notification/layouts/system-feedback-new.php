<?php
/* @var $this \yii\web\View */
/* @var $min boolean */
/* @var $model \app\modules\communication\models\Notification */
?>

<? if ($min): ?>
	<?= Yii::t('app/notification', 'system-feedback-new-text-1'); ?>
<? else: ?>
	<p class="m-b-10">
		<?= Yii::t('app/notification', 'system-feedback-new-text-1'); ?>
	</p>
	<a href="<?= $model->data->feedback_url; ?>" class="view-more" data-pjax="0">
		<i class="zmdi zmdi-long-arrow-right"></i> <?= Yii::t('app/notification', 'system-feedback-new-text-2'); ?>
	</a>
<? endif; ?>
