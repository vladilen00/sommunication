<?php
/* @var $this \yii\web\View */
/* @var $min boolean */
/* @var $model \app\modules\communication\models\Notification $model*/

use yii\helpers\Html;
?>

<? if ($min): ?>
	<?= $model->getDataLangAttribute('title'); ?>
<? else: ?>
	<p class="m-b-10"><?= $model->getDataLangAttribute('text'); ?></p>
	<?= Html::a('<i class="zmdi zmdi-more"></i> ' . Yii::t('app', 'read-more'), $model->data->url, ['class' => 'view-more', 'data-pjax' => 0]); ?>
<? endif; ?>

