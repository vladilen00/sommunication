<?php
/* @var $this \yii\web\View */
/* @var $models \app\modules\communication\models\NotificationLink[] */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\user\models\Avatar;
use app\modules\communication\models\Notification;
use app\modules\user\widgets\UserImg;
?>
<? if (!empty($models)): ?>
	<div class="list-group lg-alt">
		<? foreach ($models as $model): ?>
			<a href="<?= Url::to(['index', 'type' => $model->notification->type, '#' => $model->notification_id]); ?>" class="list-group-item media">
				<? if (!$model->viewed): ?>
					<div class="pull-right">
						<i class="zmdi zmdi-circle c-blue m-t-20 f-10"></i>
					</div>
				<? endif; ?>

				<? if ($model->notification->user !== null || $model->notification->action == Notification::ACTION_SOCIAL_TEST_ROOM_RESULT): ?>
					<div class="pull-left">
						<?= UserImg::widget(['model' => $model->notification->user, 'imageOptions' => ['class' => 'a-lg']]); ?>
					</div>
					<div class="media-body">
						<div class="lgi-heading m-b-0">
							<?= ArrayHelper::getValue($model->notification->user, 'profile.name', Yii::t('app/communication', 'views.notification..partial.last.text-1')); ?>
						</div>
						<div class="lgi-text m-b-0"><?= $model->notification->renderText(true); ?></div>
						<div class="f-10 c-gray"><?= Yii::$app->formatter->asRelativeTime($model->notification->created_at); ?></div>
					</div>
				<? elseif ($model->notification->action == Notification::ACTION_NEWS_NEW): ?>
					<div class="pull-left">
						<? if (!empty($model->notification->data->cover)): ?>
							<div class="avatar-char a-lg bg palette-Grey" style="background: url('<?= $model->notification->data->cover; ?>') center; background-size: cover; border-radius: 2px;"></div>
						<? else: ?>
							<img src="/img/avatar/internal.png" class="avatar-img a-lg"/>
						<? endif; ?>
					</div>
					<div class="media-body">
						<div class="lgi-heading m-b-0"><?= $model->notification->renderText(true); ?></div>
						<div class="lgi-text m-b-0"><?php echo Yii::t('app/communication', 'views.notification..partial.last.text-2') ?></div>
						<div class="f-10 c-gray"><?= Yii::$app->formatter->asRelativeTime($model->notification->created_at); ?></div>
					</div>
				<? endif; ?>
			</a>
		<? endforeach; ?>
	</div>
<? else: ?>
	<div class="p-20 text-center">
		<i class="zmdi zmdi-info-outline zmdi-hc-5x"></i><br/>
		<?php echo Yii::t('app/communication', 'views.notification..partial.last.text-3') ?>
	</div>
<? endif; ?>



