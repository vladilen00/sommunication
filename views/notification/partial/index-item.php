<?php
/* @var $this \yii\web\View */
/* @var $model \app\modules\communication\models\Notification */
/* @var $index integer */
/* @var $viewed boolean */

use yii\helpers\ArrayHelper;
use app\helpers\Html;
use app\modules\communication\models\Notification;
use app\modules\user\models\Avatar;
use app\modules\user\widgets\UserImg;

$deleteButton = Html::a('<i class="zmdi zmdi-close zmdi-hc-lg zmdi-hc-fw"></i>', ['delete', 'id' => $model->id], [
	'class' => 'm-l-10 c-gray comm-notification-delete_item',
	'data' => [
		'swal-confirm' => Yii::t('app/communication', 'views.notification..partial.index-item.last.text-1'),
		'swal-type' => 'info',
		'swal-method' => 'post:ajax',
	]
]);
?>
<div class="list-group-item media<?= !$viewed ? ' lg-active' : ''; ?>" id="<?= $model->id; ?>">
	<div class="pull-left checkbox p-r-5">
		<label class="m-t-5">
			<?=
			Html::checkbox(false, false, [
				'value' => $model->id,
				'class' => 'comm-notification-input_item'
			]);
			?>
		</label>
	</div>

	<? if ($model->user !== null || $model->action == Notification::ACTION_SOCIAL_TEST_ROOM_RESULT): ?>
		<div class="pull-left p-relative">
			<?= UserImg::widget(['model' => $model->user, 'link' => true, 'imageOptions' => ['class' => 'a-lg']]); ?>
		</div>
		<div class="media-body">
			<div class="clearfix m-b-5">
				<div class="pull-right c-gray">
					<?= Yii::$app->formatter->asRelativeTime($model->created_at); ?>
					<?= $deleteButton; ?>
				</div>
				<?= ArrayHelper::getValue($model->user, 'profile.name', Yii::t('app/communication', 'views.notification..partial.index-item.last.text-2')); ?>
			</div>
			<div><?= $model->renderText(); ?></div>
		</div>
	<? elseif ($model->action == Notification::ACTION_NEWS_NEW): ?>
		<div class="pull-left">
			<? if (!empty($model->data->cover)): ?>
				<div class="avatar-char a-lg bg palette-Grey" style="background: url('<?= $model->data->cover; ?>') center; background-size: cover; border-radius: 2px;"></div>
			<? else: ?>
				<img src="/img/avatar/internal.png" class="avatar-img a-lg"/>
			<? endif; ?>
		</div>
		<div class="media-body">
			<div class="clearfix m-b-5">
				<div class="pull-right c-gray">
					<?= Yii::$app->formatter->asRelativeTime($model->created_at); ?>
					<?= $deleteButton; ?>
				</div>
				<strong><?= $model->getDataLangAttribute('title'); ?></strong>
			</div>
			<?= $model->renderText(); ?>
		</div>
	<? endif; ?>
</div>

