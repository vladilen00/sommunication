<?php
/* @var $this \yii\web\View */
/* @var $model \app\modules\communication\models\NotificationLink */
/* @var $dataProvider \yii\data\ActiveDataProvider */

use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ListView;
use app\helpers\Html;
use app\modules\communication\models\Notification;

$this->title = Yii::t('app/communication', 'views.notification.text-1') . ' | ' . Yii::$app->name;
$this->params['breadcrumbs'][] = Yii::t('app/communication', 'views.notification.text-1');
?>

<div class="card" id="comm-notification-index">
	<ul class="tab-nav text-center">
		<? foreach (Notification::$types as $type => $icon): ?>
			<li<?= $model->type == $type ? ' class="active"' : ''; ?>>
				<a href="<?= Url::to(['index', 'type' => $type]); ?>" data-target="#" data-toggle="tab">
					<i class="zmdi zmdi-hc-3x zmdi-hc-fw zmdi-<?= $icon; ?>"></i>
				</a>
			</li>
		<? endforeach; ?>
	</ul>
	<? $pjax = Pjax::begin(['options' => ['class' => 'pjax-loader pjax-scroll']]); ?>
	<?=
	ListView::widget([
		'dataProvider' => $dataProvider,
		'options' => ['class' => 'list-group lg-alt list-view'],
		'itemOptions' => ['tag' => false],
		'itemView' => function ($model, $key, $index) {
			return $this->render('partial/index-item', ['model' => $model->notification, 'index' => $index, 'viewed' => $model->viewed]);
		},
		'emptyText' => '<i class="zmdi zmdi-info-outline zmdi-hc-5x"></i><br/> ' .Yii::t('app/communication', 'views.notification.text-2'),
		'emptyTextOptions' => ['class' => 'p-30 text-center'],
		'summary' => false,
		'beforeItem' => function ($model, $key, $index) {
			$out = '';

			if ($index === 0) {
				$out .= Html::beginTag('div', ['class' => 'list-group-item media p-t-5 p-b-5']);
				$out .= Html::tag('div', Html::tag('label', Html::checkbox(false, false, ['class' => 'comm-notification-select_all']) . Yii::t('app/communication', 'views.notification.text-3')), ['class' => 'pull-left checkbox']);
				$out .= Html::beginTag('div', ['class' => 'media-body p-t-10 p-b-10']);
				$out .= Html::a('<i class="zmdi zmdi-delete zmdi-hc-lg zmdi-hc-fw"></i> ' . Yii::t('app/communication', 'views.notification.text-4'), ['delete'], [
					'class' => 'text palette-Grey-600 hidden comm-notification-delete_selected',
					'data' => [
						'swal-confirm' => Yii::t('app/communication', 'views.notification.text-5'),
						'swal-type' => 'info',
						'swal-method' => 'false',
					]
				]);
				$out .= Html::endTag('div');
				$out .= Html::endTag('div');
			}
			$out .= '<hr class="m-0">';
			return $out;
		},
	]);
	?>
	<? Pjax::end(); ?>
</div>
<script>
	window.app.ready.add(function () {
		$('#comm-notification-index .tab-nav a').on('show.bs.tab', function () {
			$.pjax.reload({
				container: '#<?= $pjax->id; ?>',
				url: $(this).attr('href')
			});
		});

		$('#comm-notification-index')
				.on('change', '.comm-notification-select_all', function (e) {
					$('.comm-notification-input_item').prop('checked', this.checked).first().trigger('change');
				})
				.on('change', '.comm-notification-input_item', function () {
					$('.comm-notification-select_all').prop('checked', $('.comm-notification-input_item:not(:checked)').length == 0);
					$('.comm-notification-delete_selected').toggleClass('hidden', $('.comm-notification-input_item:checked').length == 0);
				})
				.on('swal.confirm', '.comm-notification-delete_selected', function () {
					var values = $('.comm-notification-input_item:checked').map(function () {
						return this.value;
					}).toArray();
					var url = $(this).attr('href') + '?' + $.param({id: values});

					$.post(url, function (response) {
						if (response.success) {
							$.pjax.reload({container: '#<?= $pjax->id; ?>'});
						}
					});
				});
				
		$('#<?= $pjax->id; ?>').on('pjax:end', function () {
			$('.comm-notification-last-trigger').trigger('refresh.app.comm');
		});
	});
</script>


