<?php

use yii\helpers\Url;

/**
 * @var app\modules\communication\models\Notification $model
 */

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Запрос на добавление в круги</title>
</head>
<body>
    <table style="width: 480px; margin: 0 auto; font-family: Tahoma; text-align: center;">
        <tr>
            <td style="padding: 20px;"><img src="<?php echo Url::to('@web/img/email-logo.png', true)?>" style="width: 120px;"></td>

        <tr style="font-size: 16px; font-weight: 600; text-transform: uppercase; color: #2196F3;">
            <td style="padding: 15px;">Приглашение пройти тест</td>

        <tr style="font-size: 14px; color: #555;">
            <td style="padding-bottom: 20px;">Пользователь
                <a href="<?php echo Url::to(['/user/profile/view', 'id' => $model->user->id], true)?>" style="color: #2196F3"><?php echo $model->user->profile->name ?></a>
                приглашает Вас пройти тест:
                <p><b><?php echo $model->data->test_name_ru ?></b></p>
            </td>

        <tr style="">
            <td style="padding-top: 28px;">
                <a href="<?php echo Url::to($model->data->room_url, true)?>" style="width:150px; margin: 0 auto; padding: 8px 20px; font-weight: 600; font-size: 13px; text-decoration: none; text-transform: uppercase; color: #fff; background-color: #2196F3;">
                    Пройти тест
                </a>
            </td>

        <tr style="font-size: 13px; color: #8e8e8e;">
            <td style="padding-top: 40px;">С уважением, администрация <b>Psyguru.</b></td>

        <tr style="font-size: 12px; color: #8e8e8e;">
            <td>Если вы хотите отписаться от подобных писем, перейдите по <a href="<?php echo Url::to(['/communication/notification/unsubscribe'], true) ?>" style="color: #2196F3">ссылке.</a></td>
    </table>
</body>
</html
