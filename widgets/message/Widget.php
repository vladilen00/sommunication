<?php

namespace app\modules\communication\widgets\message;

use yii\helpers\Html;
use app\modules\communication\models\Sender;

class Widget extends \yii\base\Widget {

	public $target;
	public $to;
	public $inputOptions = ['rows' => 2];

	public function run() {
		Html::addCssClass($this->inputOptions, 'comm-dialog-message-field form-control');

		$model = new Sender;
		$model->target = $this->target;
		$model->to = $this->to;

		if (!isset($this->inputOptions['placeholder']))
			$this->inputOptions['placeholder'] = $model->getAttributeLabel('text');
		
		$this->inputOptions['style'] = 'font-size: 14px;';

		return $this->render('form', ['model' => $model]);
	}

}
