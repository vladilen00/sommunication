<?php
/* @var $this \yii\web\View */
/* @var $model \app\modules\communication\models\Sender */

use yii\helpers\Html;
use app\widgets\ActiveForm;

$form = ActiveForm::begin([
	'id' => 'comm-dialog-message-form',
	'action' => ['/communication/message/send'],
	'validateOnChange' => false,
	'validateOnBlur' => false,
	'successCssClass' => '',
	'errorCssClass' => '',
	'fieldConfig' => [
		'errorOptions' => [
			'class' => 'help-block help-block-error'
		]
	]
]);
echo Html::activeHiddenInput($model, 'target');
echo Html::activeHiddenInput($model, 'to');

echo $form->beginField($model, 'text', ['options' => ['class' => '']]);
?>
<div class="input-group">
<?= Html::activeTextarea($model, 'text', $this->context->inputOptions); ?>
	<a href="#" class="input-group-addon btn btn-primary-light submit-form" title="<?= $model->getAttributeHint('text'); ?>" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px; border-left: 0px;">
		<span class="icon fa-paper-plane-o"></span>
	</a>
</div>
<div class="help-block help-block-error small text-danger" style="margin-bottom: 0;"></div>
<? echo $form->endField(); ?>

<? $form->end(); ?>
<script>
	window.app.ready.add(function () {
		$('#comm-dialog-message-form')
				.on('click', '.submit-form', function (e) {
					e.preventDefault();
					$(e.delegateTarget).submit();
				})
				.on('beforeSubmit', function (e) {
					$(this).find('.submit-form').addClass('disabled');

					$.ajax({
						url: $(this).attr('action'),
						data: $(this).serialize(),
						type: $(this).attr('method'),
						dataType: 'json',
						success: function (response) {
							if (response.success) {
								$(e.target).find('textarea').val(null);
								$(e.target).trigger('app.dialog.message.sended');
							} else {
								$(e.target).yiiActiveForm('updateAttribute', 'sender-text', response.data.messages);
							}
						},
						error: function () {
							$(e.target).yiiActiveForm('updateAttribute', 'sender-text', ['<?php echo Yii::t('app/communication', 'widgets.message.view.form.text-1')?>']);
						},
						complete: function () {
							$(e.target).find('.submit-form').removeClass('disabled');
							$(e.target).find('textarea').focus();
						}
					});
					return false;
				})
				.on('keydown', 'textarea', function (e) {
					if (e.keyCode == 13) {
						if (e.ctrlKey) {
							$(this).val($(this).val() + "\n");
						} else {
							e.preventDefault();
							$(e.delegateTarget).find('.submit-form').trigger('click');
						}
					}
				});
	});
</script>

