<?php
/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<?= Html::a('<span class="icon fa-bell-o"></span>', $this->context->getCountUrl, ['class' => 'header_notice_link flex -middle open-dropdown counter']); ?>

<div class="dropdown dropdown-notice">
	<div class="dropdown-notice_head flex -x_between -y_center">
		<h4><?php echo Yii::t('app/communication', 'views.notify.text-1') ?></h4>
		<!--a href="#">настройки</a-->
	</div>
	<!--div class="dropdown-notice_footer">
		<a href="#">Посмотреть всe</a>
	</div-->
</div>

<script>
	var notify = function () {
		var last = 0;

		$('.dropdown-notice').on('app.dropdown.open', function (e) {
			var now = new Date().getTime();

			if (last === 0) {
				last = now;
			} else {
				if (now - last < <?= intval($this->context->updateMinInterval); ?>)
					return;
				
				last = now;
			}
			var $head = $(this).children('.dropdown-notice_head');

			$head.next('ul').remove();
			$head.after('<ul><li class="flex -y_center"><span class="icon fa-refresh"></span> <?php echo Yii::t('app/communication', 'views.notify.text-2')?> </li></ul>');

			$.get('<?= Url::to($this->context->getLastUrl); ?>', function (response) {
				$(e.target).children('ul').replaceWith(response);
			});

			setTimeout(function () {
				$('.header_notice_link').trigger('app.counter.get');
			}, <?= intval($this->context->updateCounterDelay); ?>);
		});
	};
</script>
<?
$this->registerJs('notify();');


