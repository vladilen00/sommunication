<?php

namespace app\modules\communication\widgets;

use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class HeaderNotice extends \yii\base\Widget {

	public $options = [];
	public $dropDownOptions = [];
	public $linkOptions = [];
	public $linkUrl;
	public $linkText;
	public $dropDownUrl;
	public $delay = 250;
	public $interval = 10000;

	public function init() {
		if (!isset($this->options['id']))
			$this->options['id'] = $this->id;

		Html::addCssClass($this->options, 'dropdown');
		Html::addCssClass($this->dropDownOptions, 'dropdown-menu');

		if (!isset($this->linkOptions['data-toggle']) || (isset($this->linkOptions['data']) && !isset($this->linkOptions['data']['toggle'])))
			$this->linkOptions['data']['toggle'] = 'dropdown';

		if (is_array($this->dropDownUrl))
			$this->dropDownUrl = Url::to($this->dropDownUrl);
	}

	public function run() {
		$this->registerJs();

		$containerTag = ArrayHelper::remove($this->options, 'tag', 'div');
		$dropDownTag = ArrayHelper::remove($this->dropDownOptions, 'tag', 'div');

		$out = '';
		$out .= Html::beginTag($containerTag, $this->options);
		$out .= Html::a($this->linkText, $this->linkUrl, $this->linkOptions);
		$out .= Html::tag($dropDownTag, '', $this->dropDownOptions);
		$out .= Html::endTag($containerTag);
		return $out;
	}

	protected function registerJs() {
		$this->view->registerJs("app.notification.add(" . Json::encode([
			'selector' => "#{$this->options['id']}",
			'url' => $this->dropDownUrl,
			'interval' => $this->interval,
			'delay' => $this->delay
		]) . ");");
	}

}
