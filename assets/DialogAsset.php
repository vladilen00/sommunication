<?php

namespace app\modules\communication\assets;

class DialogAsset extends \yii\web\AssetBundle {
	
	public $sourcePath = '@app/modules/communication/assets';
	public $js = [
		'js/dialog.js',
	];
	public $publishOptions = [
		'except' => ['*.php'],
		'forceCopy' => YII_DEBUG
	];
	public $depends = [
		'yii\web\JqueryAsset'
	];
}
