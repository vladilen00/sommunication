/**
 * Где-то в мире плачет один web-socket сервер
 */
$(function () {
	/**
	 * Минимальная высота контейнера с сообщениями при ресайзе окна
	 * @type Number
	 */
	var AUTOSIZE_MIN_HEIGHT = 300;

	/**
	 * Высота лоадера при выполнении действий
	 * @type Number
	 */
	var LOADER_HEIGHT = 69;

	/**
	 * Скорость выполнения анимации
	 * @type String|Number
	 */
	var TRANSITION = 80;

	/**
	 * Интервал между запросами для загрузки новых сообщений
	 * @type Number
	 */
	var MESSAGE_UPDATE_INTERVAL_TIMEOUT = 6000;

	/**
	 * Интервал ожидания после последней отправки сообщения, чтобы не спамили
	 * @type Number
	 */
	var SUBMIT_FORM_ENABLED_TIMEOUT = 800;

	var SCROLL_UP = -1;
	var SCROLL_INIT = 0;
	var SCROLL_DOWN = 1;

	var messageUpdateInterval = null;

	/**
	 * @type Request
	 */
	var request = new Request();

	/**
	 * Запросы, выполнющиеся с приоритетом
	 * @returns {dialogL#4.Request}
	 */
	function Request() {
		/**
		 * Текущий запрос
		 * @type {XMLHttpRequest}
		 */
		var _xhr = null;

		/**
		 * Приоритет текущего запроса
		 * @type Number
		 */
		var _priority = 0;

		/**
		 * Сделать запрос
		 * @param {Number} priority - Приоритет. Чем больше число, тем выше приоритет
		 * @param {Object} options - Опции для $.ajax
		 * @param {Function} callback - Callback, если текущий запрос не может быть выполнен 
		 * по причине низкого (или равного) приоритета перед выполняющимся в данный момент запросом
		 * @returns {dialogL#4.Request}
		 */
		this.make = function (priority, options, callback) {
			if (_xhr !== null && _xhr.readyState < 4) {
				if (_priority < priority) {
					this.abort();
				} else {
					if (typeof callback === 'function') {
						callback(_priority);
					}
					return this;
				}
			}
			_priority = priority;

			if (options.type === undefined) {
				options.type = 'GET';
			}

			if (options.dataType === undefined) {
				options.dataType = 'json';
			}
			_xhr = $.ajax(options).always(function () {
				_xhr = null;
				_priority = 0;
			});
			return this;
		};

		/**
		 * Отменить выполнение текущего запроса
		 * @returns {dialogL#4.Request}
		 */
		this.abort = function () {
			if (_xhr !== null && _xhr.readyState < 4) {
				_xhr.abort();
			}
			return this.reset();
		};

		/**
		 * Очистить информацию о текущем запросе
		 * @returns {dialogL#4.Request}
		 */
		this.reset = function () {
			_xhr = null;
			_priority = 0;
			return this;
		};
		return this;
	}

	/**
	 * Показать сообщение
	 * @param {String} text
	 * @returns {undefined}
	 */
	var showNotify = function (text) {
		$.notifyClose('all');
		$.notify(text, {
			type: 'inverse',
			placement: {
				from: 'bottom',
				align: 'center'
			},
			animate: {
				enter: 'animated fadeInUp',
				exit: 'animated fadeOutDown'
			},
			delay: 3000
		});
	};

	/**
	 * Включить автоматическую загрузку новых сообщений
	 * @param {String} url
	 * @returns {undefined}
	 */
	var setMessageUpdateInterval = function (url) {
		messageUpdateInterval = setInterval(function () {
			request.make(1, {
				url: url,
				data: {
					s: SCROLL_DOWN,
					m: $('.comm-message-list-item:last').data('key')
				},
				success: function (response) {
					var scrollBottom = $('#comm-message-list').scrollTop() + $('#comm-message-list').height() == getListHeight();

					$(response.data.html).hide().appendTo('#comm-message-list').fadeIn(TRANSITION);

					if (response.count > 0 && scrollBottom) {
						$('#comm-message-list').scrollTop(1E10);
					}
					$.pjax.reload('#comm-dialog-container', {push: false, timeout: 0});
				}
			});
		}, MESSAGE_UPDATE_INTERVAL_TIMEOUT);
	};

	/**
	 * Отключить автоматическую загрузку новых сообщений
	 * @returns {undefined}
	 */
	var unsetMessageUpdateInterval = function () {
		if (messageUpdateInterval !== null) {
			clearInterval(messageUpdateInterval);
			messageUpdateInterval = null;
		}
	};

	/**
	 * Лоадер для выполнения действий
	 * @returns {jQuery}
	 */
	var getLoader = function () {
		return $('<div>').addClass('p-15 text-center').css('height', LOADER_HEIGHT).html('<i class="zmdi zmdi-refresh zmdi-hc-spin zmdi-hc-3x"></i>');
	};

	/**
	 * Получить суммарную высоту всех сообщений у #comm-message-list
	 * @returns {Number}
	 */
	var getListHeight = function () {
		var height = 0;

		$('#comm-message-list > .comm-message-list-item').each(function () {
			height += $(this).outerHeight(true);
		});
		return height;
	};

	/**
	 * Загрузить предыдущие сообщения при скроллинге вверх
	 * @param {$.Event} e
	 * @returns {undefined}
	 */
	var loadPrevMessages = function (e) {
		var height, $loader;

		request.make(2, {
			url: e.data.url,
			data: {
				s: SCROLL_UP,
				m: $('.comm-message-list-item:first').data('key')
			},
			beforeSend: function () {
				$('#comm-message-load-more').remove();
				height = getListHeight();
				$loader = getLoader().hide().prependTo(e.target).fadeIn(TRANSITION);
			},
			success: function (response) {
				$loader.fadeOut(TRANSITION, function () {
					$(response.data.html).hide().replaceAll(this).fadeIn(TRANSITION);

					if (response.count > 0) {
						$(e.target).scrollTop(getListHeight() - height - LOADER_HEIGHT);
					} else {
						$(e.target).off('scroll.dialog');
					}
				});
			},
			error: function () {
				$loader.remove();
			}
		}, function () {
			$(e.target).scrollTop(1);
		});
	};

	/**
	 * Обработчик при скроллинге #comm-message-container
	 * @param {$.Event} e
	 * @returns {undefined}
	 */
	var scrollHandler = function (e) {
		if ($(this).scrollTop() === 0) {
			$(this).trigger('prev.app.dialog');
		}
	};

	/**
	 * Отключить/включить обработчик формы
	 * @param {Boolean} state
	 * @returns {undefined}
	 */
	var setSubmit = function (state) {
		if (state) {
			$('#comm-message-form').off('submit').on('submit', submitForm);
		} else {
			$('#comm-message-form').off('submit').on('submit', false);
		}
	};

	/**
	 * После валидации  формы/перед отправкой формы
	 * @param {$.Event} e
	 * @returns {Boolean}
	 */
	var submitForm = function (e) {
		e.preventDefault();

		var text = $(this).children('textarea').val();

		if (text.length === 0) {
			showNotify(window.app.t("must-enter-text"));
			return;
		}
		var $loader;

		// Отправить сообщение
		request.make(4, {
			url: $(this).attr('action'),
			type: $(this).attr('method'),
			data: $(this).serialize(),
			beforeSend: function () {
				setSubmit(false);
				$loader = getLoader().hide().appendTo('#comm-message-list').fadeIn(TRANSITION);
				$('#comm-message-list').scrollTop(1E10);
			},
			success: function (res1) {
				if (res1.success) {
					$('#comm-message-form textarea').val(null);

					// Загрузить список последних сообщений
					request.make(2, {
						reset: false,
						url: res1.data.url,
						data: {
							s: SCROLL_DOWN,
							m: $('.comm-message-list-item:last').data('key')
						},
						success: function (res2) {
							$loader.fadeOut(TRANSITION, function () {
								$(res2.data.html).hide().replaceAll(this).fadeIn(TRANSITION);
								$('#comm-message-list').scrollTop(1E10);
							});
						},
						error: function () {
							$loader.remove();
							showNotify(window.app.t("error-ref-page"));
						},
						complete: function () {
							setTimeout(function () {
								setSubmit(true);
							}, SUBMIT_FORM_ENABLED_TIMEOUT);
						}
					});
				} else {
					$loader.remove();
					showNotify(res1.data.message);
					setSubmit(true);
				}
			},
			error: function () {
				$loader.remove();
				showNotify(window.app.t("error-send-message"));
				setSubmit(true);
			}
		});
	};

	/**
	 * Изменение размера списка с сообщениями при изменении размера окна браузера
	 * @returns {undefined}
	 */
	var resize = function () {
		if ($('#comm-message-list').length > 0) {
			var height = $(window).height() - $('#comm-message-list').offset().top - 100;

			if (height < AUTOSIZE_MIN_HEIGHT) {
				height = AUTOSIZE_MIN_HEIGHT;
			}
			$('#comm-message-list').css({'max-height': height, 'overflow-y': 'scroll'});
		}
	};

	/**
	 * Клик по диалогу в левом меню
	 */
	$('#comm-dialog-container').on('click', '#comm-dialog-list > .comm-dialog-list-item', function (e) {
		e.preventDefault();

		request.make(3, {
			url: $(this).attr('href'),
			beforeSend: function () {
				unsetMessageUpdateInterval();
			},
			success: function (response) {
				$('#comm-message-container').html(response.data.html);
				resize();
				$('#comm-message-list').scrollTop(response.data.scroll == 0 ? 1 : 1E10);
				$('#comm-message-list').on('scroll.dialog', scrollHandler);
				$('#comm-message-list').on('prev.app.dialog', {url: response.data.url}, loadPrevMessages);

				$('#comm-message-form').on('submit', submitForm);

				setMessageUpdateInterval(response.data.url);
			},
			error: function () {
				showNotify(window.app.t("could-not-retrieve-messages"));
			}
		});
		$('[data-ma-action="message-toggle"].toggled').trigger('click');
	});

	/**
	 * По нажатию Enter - отправить сообщение
	 * По нажатию Ctrl+Enter - Новая строка
	 */
	$('#comm-message-container').on('keydown', '#comm-message-form textarea', function (e) {
		if (e.keyCode === 13) {
			if (e.ctrlKey) {
				$(this).val($(this).val() + "\n");
			} else {
				e.preventDefault();
				$(this).closest('form').submit();
			}
		}
	});

	/**
	 * Загрузить предыдущие сообщения по нажатию на кнопку "Загрузить предыдущие сообщения" 
	 */
	$('#comm-message-container').on('click', '#comm-message-load-more', function (e) {
		e.preventDefault();
		$('#comm-message-list').trigger('prev.app.dialog');
	});

	/**
	 * Выделить/снять выделение сообщение/я
	 */
	$('#comm-message-container').on('click', '.comm-message-list-item .msb-item', function () {
		var $checkbox = $(this).closest('.comm-message-list-item').find('.comm-message-list-item-input');
		$checkbox.prop('checked', !$checkbox.prop('checked')).trigger('change');
	});

	/**
	 * При выделении/снятии выделения сообщения показать/скрыть кнопку удаления сообщений
	 */
	$('#comm-message-container').on('change', '.comm-message-list-item-input', function (e) {
		if ($('.comm-message-list-item-input:checked').length > 0) {
			$('#comm-message-hide').stop(true, true).fadeIn(TRANSITION);
		} else {
			$('#comm-message-hide').stop(true, true).fadeOut(TRANSITION);
		}
	});

	/**
	 * Нажатие на кнопку для удаления выделенных сообщений
	 */
	$('#comm-message-container').on('swal.confirm', '#comm-message-hide', function () {
		var values = $('.comm-message-list-item-input:checked').map(function () {
			return this.value;
		}).toArray();

		if (values.length === 0) {
			return;
		}
		request.make(3, {
			url: $(this).attr('href'),
			type: 'POST',
			data: {id: values},
			success: function (response) {
				if (response.success) {
					$('#comm-message-hide').fadeOut(TRANSITION);
					$('.comm-message-list-item-input:checked').closest('.comm-message-list-item').fadeOut(TRANSITION, function () {
						$(this).remove();
					});
				}
			}
		});
	});

	/**
	 * Запросить кол-во новых сообщений для отображения над меню (значок новых сообщений)
	 */
	$('#comm-dialog-container').on('pjax:end', function () {
		$('.comm-message-last-trigger').trigger('refresh.app.comm');
	});

	/**
	 * Открыть диалог по ссылке (при переходе на диалог по "прямой" ссылке)
	 */
	$('#comm-dialog-list > .comm-dialog-list-item.initial').trigger('click');

	/**
	 * Изменять высоту #comm-message-container при изменении размера окна
	 */
	$(window).on('resize.dialog', function () {
		resize();
	});
});



//(function (app) {
//	var selectors = {
//		container: '#comm-message-container',
//		list: '#comm-message-container',
//		item: '.comm-dialog-message-item',
//		form: '#comm-dialog-message-form',
//		hm: '#comm-dialog-message-hide',
//		hd: '#comm-dialog-hide'
//	};
//
//	app.ready.add(function () {
//		$(selectors.form).on('app.dialog.message.sended', function () {
//			scrollLoad('down');
//		});
//		$(selectors.hm).on('click', hideMessages);
//		$(selectors.hd).on('click', function (e) {
//			app.modal.confirm({
//				title: 'Удаление переписки',
//				text: 'Вы действительно хотите удалить всю переписку?',
//				size: 'sm',
//				onPositive: function () {
//					window.location = e.target.href;
//				}
//			});
//			e.preventDefault();
//		});
//		$(selectors.list)
//				.on('click', selectors.item, selectMessage)
//				.on('click mouseenter', selectors.item + '.new', readMessage);
//		$(selectors.container)
//				.on('app.dialog.scroll.loaded', function (e, direction) {
//					addDividers(direction);
//				})
//				.on('app.dialog.scroll.load.off', function () {
//					addFirstDivider();
//				})
//				.on('app.dialog.message.hidden', function () {
//					removeDividers();
//					addDividers('down');
//
//					if ($(this).scrollTop() == 0) {
//						addFirstDivider();
//					}
//				});
//
//		setInterval(function () {
//			$.get($(selectors.list).data('counter-url'), function (response) {
//				if (response.count > 0) {
//					scrollLoad('down');
//				}
//			}, 'json');
//		}, $(selectors.list).data('counter-interval'));
//
//		$(window).on('resize.app.dialog', function () {
//			var newHeight = $(window).height() - $(selectors.container).offset().top - 177;
//
//			if (newHeight < 220)
//				newHeight = 220;
//
//			$(selectors.container).height(newHeight);
//		}).trigger('resize.app.dialog');
//
//		addDividers('down');
//
//		scrollDown();
//
//		if ($(selectors.container).scrollTop() > 0) {
//			$(selectors.container).on('scroll', function () {
//				if ($(this).scrollTop() == 0) {
//					scrollLoad('up');
//				}
//			});
//		} else {
//			addFirstDivider();
//		}
//	});
//
//	var _date = {up: null, down: null};
//
//	var addDividers = function (direction) {
//		var $elements;
//
//		if (direction == 'down') {
//			$elements = $(selectors.item);
//		} else {
//			$elements = $($(selectors.item).get().reverse());
//		}
//		$elements.not('.divided').each(function () {
//			var date = $(this).data('date');
//
//			if (_date[direction] === null) {
//				_date[direction] = date;
//			} else if (_date[direction] != date) {
//				var $divider = $('<div class="date-divider text-center text-muted small"><hr></div>');
//
//				if (direction == 'down') {
//					$(this).before($divider.prepend(date));
//				} else {
//					$(this).after($divider.prepend(_date[direction]));
//				}
//				_date[direction] = date;
//			}
//			$(this).addClass('divided');
//		});
//	};
//
//	var addFirstDivider = function () {
//		var $item = $(selectors.item).first();
//		$item.before('<div class="date-divider text-center text-muted small">' + $item.data('date') + '<hr></div>');
//	};
//
//	var removeDividers = function () {
//		$(selectors.item + '.divided').removeClass('divided');
//		$(selectors.list).find('.date-divider').remove();
//		_date = {up: null, down: null};
//	};
//
//	var counter = null;
//
//	var updateCounter = function () {
//		if (counter !== null) {
//			clearTimeout(counter);
//			counter = null;
//		}
//		counter = setTimeout(function () {
//			$('.comm-dialog-link').trigger('app.counter.get');
//		}, 2000);
//	};
//
//	var scrollDown = function () {
//		$(selectors.container).scrollTop(1E10);
//	};
//
//	var scrollLoad = function (direction) {
//		var $container = $(selectors.container);
//
//		if ($container.length === 0 || $container.hasClass('scrollLoading'))
//			return;
//
//		$container.addClass('scrollLoading');
//
//		var message, height,
//				$list = $(selectors.list),
//				$loader = $('<div class="loader"></div>').hide();
//
//		if (direction == 'down') {
//			$loader.appendTo($list).fadeIn();
//			message = $list.children(selectors.item).last().data('key');
//		} else {
//			$loader.prependTo($list).fadeIn();
//			message = $list.children(selectors.item).first().data('key');
//		}
//		height = $list.height();
//
//		$.ajax({
//			url: window.location,
//			data: {message: message, direction: direction},
//			success: function (response) {
//				if (response.count > 0) {
//					if (direction == 'down') {
//						$list.append(response.data.html);
//						scrollDown();
//					} else {
//						$list.prepend(response.data.html);
//						$container.scrollTop($list.height() - height - $loader.height());
//					}
//					$container.find('.empty-text').remove();
//				} else {
//					if (direction == 'up') {
//						$container.off('scroll');
//						$container.trigger('app.dialog.scroll.load.off');
//					}
//				}
////				updateCounter();
//			},
//			error: function () {
//
//			},
//			complete: function () {
//				$loader.remove();
//				$container.trigger('app.dialog.scroll.loaded', [direction]);
//				$container.removeClass('scrollLoading');
//			}
//		});
//	};
//
//	var hideMessages = function (e) {
//		e.preventDefault();
//
//		var url = $(this).attr('href');
//
//		app.modal.confirm({
//			title: 'Удаление сообщений',
//			text: 'Вы действительно хотите удалить выбранные сообщения?',
//			size: 'sm',
//			onPositive: function () {
//				var ids = $(selectors.item + '.active').map(function () {
//					return $(this).data('key');
//				}).toArray();
//
//				if (ids.length > 0) {
//					$.post(url, {msgs: ids}, function (response) {
//						if (response.success) {
//							$(selectors.item + '.active').fadeOut(function () {
//								$(this).remove();
//								$(selectors.container).trigger('app.dialog.message.hidden');
//							});
//						}
//					}, 'json');
//				}
//				$(this).modal('hide');
//			}
//		});
//	};
//
//	var selectMessage = function () {
//		$(this).toggleClass('active');
//
//		if ($(selectors.item + '.active').length === 0) {
//			$(selectors.hm).addClass('disabled');
//		} else {
//			$(selectors.hm).removeClass('disabled');
//		}
//	};
//
//	var readMessage = function () {
//		$(this).removeClass('new');
//	};
//});


