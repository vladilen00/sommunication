<?php

namespace app\modules\communication\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\communication\models\Message;
use app\modules\communication\models\Sender;

class MessageController extends \app\components\AccountController {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['access'] = [
			'class' => 'yii\filters\AccessControl',
			'rules' => [
				[
					'allow' => true,
					'roles' => ['@'],
				]
			],
		];
		$behaviors['verbs'] = [
			'class' => 'yii\filters\VerbFilter',
			'actions' => [
				'send' => ['post'],
				'hide' => ['post'],
			]
		];
		return $behaviors;
	}

	/**
	 * Поиск по сообщениям
	 * @param string $q - Запрос
	 * @param integer $dialog_id - ID диалога, если надо сузить поиск до одного диалога
	 */
	public function actionSearch($q, $dialog_id = null) {
		$query = Message::find();
		$query->addParams(['uid' => Yii::$app->user->id]);

		if (!empty($dialog_id)) {
			$query->fromDialog($dialog_id);
		} else {
			$query->fromActiveDialogs()->notHidden();
		}
		$query->orderBy(['comm_dialog_message.created_at' => SORT_DESC]);

		if (empty($q))
			$query->andWhere('0=1');

		if (!empty($dialog_id))
			$query->andWhere(['comm_dialog_message.dialog_id' => $dialog_id]);

		$query->andWhere(['like', 'text', $q]);
		$dataProvider = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
		return $this->render('search', ['dataProvider' => $dataProvider]);
	}

	/**
	 * Отправить сообщение пользователю. При надобности будет создан новый диалог.
	 */
	public function actionSend($id) {
		Yii::$app->response->format = 'json';

		$model = new Sender;
		$model->from = Yii::$app->user->id;
		$model->to = $id;
		$model->target = Sender::TARGET_USER;

		if ($model->load(Yii::$app->request->post()) && $model->send()) {
			return ['success' => true];
		} else {
			return ['success' => false, 'data' => ['messages' => array_values($model->errors)]];
		}
	}

	/**
	 * Скрыть сообщения
	 */
	public function actionHide() {
		Yii::$app->response->format = 'json';

		$query = Message::find();
		$query->addParams(['uid' => Yii::$app->user->id]);
		$query->select('comm_dialog_message.id');
		$query->andWhere(['comm_dialog_message.id' => Yii::$app->request->post('id')]);
		$query->fromActiveDialogs()->notHidden();
		$ids = $query->createCommand()->queryColumn();

		if (empty($ids))
			return ['success' => false];

		$rows = array_map(function ($id) {
			return [
				$id,
				Yii::$app->user->id,
			];
		}, $ids);
		Yii::$app->db->createCommand()->batchInsert('comm_dialog_message_hidden', ['message_id', 'user_id'], $rows)->execute();
		return ['success' => true];
	}

	/**
	 * Получить кол-во непрочтенных сообщений (в диалоге, если ID диалога указан)
	 * @param integer $dialog_id
	 */
	public function actionGetCount($dialog_id = null) {
		Yii::$app->response->format = 'json';

		$query = Message::find()->notHidden()->onlyNew()->fromActiveDialogs();
		$query->addParams(['uid' => Yii::$app->user->id]);

		if (!empty($dialog_id))
			$query->andWhere(['comm_dialog_message.dialog_id' => $dialog_id]);

		return ['count' => $query->count()];
	}

}
