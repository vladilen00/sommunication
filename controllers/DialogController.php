<?php

namespace app\modules\communication\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use app\modules\communication\models\Sender;
use app\modules\communication\models\Dialog;
use app\modules\communication\models\Message;
use app\modules\communication\models\DialogLink;

class DialogController extends \app\components\AccountController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors           = parent::behaviors();
        $behaviors['access'] = [
            'class' => 'yii\filters\AccessControl',
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ],
        ];
        $behaviors['verbs']  = [
            'class'   => 'yii\filters\VerbFilter',
            'actions' => [
                'send' => ['post'],
                'hide' => ['post'],
            ]
        ];

        return $behaviors;
    }

    /**
     * Запрос для списка диалогов
     * @return \yii\db\ActiveQuery
     */
    private function getDialogQuery()
    {
        $query = Dialog::find();
        $query->addParams(['uid' => Yii::$app->user->id]);
        $query->sortByLastMessage(true);
        $query->with([
            'users' => function ($query) {
                $query->except();
            },
            'users.profile.file',
            'lastMessage',
        ]);
        $query->groupBy(['comm_dialog.id']);

        return $query;
    }

    /**
     * Список диалогов пользователя
     */
    public function actionIndex($id = null)
    {
        $model        = new Sender;
        $dataProvider = new ActiveDataProvider([
            'query'      => $this->getDialogQuery(),
            'pagination' => [
                'pageSize' => 15,
            ]
        ]);

        return $this->render('index', ['dataProvider' => $dataProvider, 'model' => $model, 'dialogId' => $id]);
    }

    /**
     * Список последнийх диалогов для dropdown
     */
    public function actionLast()
    {
        $query = $this->getDialogQuery();
        $query->limit(8);

        return $this->renderPartial('partial/last', ['models' => $query->all()]);
    }

    /**
     * Перейти к диалогу. Перед этим найти последнее непрочитанное сообщение начать показ сообщений с него.
     *
     * @param integer $id
     */
    public function actionEnter($id)
    {
        /* @var $model Dialog */
        $query = Dialog::find();
        $query->addParams(['uid' => Yii::$app->user->id]);
        $query->with([
            'users' => function ($query) {
                $query->except();
            },
            'users.profile.file',
        ]);
        $query->active();
        $query->andWhere(['comm_dialog.id' => $id]);
        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException;
        }
        $message = $model->getMessages()->addParams(['uid' => Yii::$app->user->id])->onlyNew()->limit(1)->one();

        if ($message !== null) {
            $message->scroll = Message::SCROLL_INIT;
        } else {
            $message = new Message([
                'dialog_id' => $model->id,
                'scroll'    => Message::SCROLL_UP,
            ]);
        }
        $message->user_id = Yii::$app->user->id;
        $dataProvider     = $message->search();

        return $this->asJson([
            'success' => true,
            'data'    => [
                'url'    => Url::to(['view', 'id' => $id]),
                'scroll' => $message->scroll,
                'html'   => $this->renderPartial('partial/enter', [
                    'model'   => $model,
                    'sender'  => new Sender,
                    'content' => $this->renderPartial('partial/view', ['dataProvider' => $dataProvider])
                ]),
            ],
        ]);
    }

    /**
     * Просмотр сообщений в диалоге
     *
     * @param integer $id - ID диалога
     * @param integer $m
     * @param integer $s
     *
     * @throws NotFoundHttpException
     */
    public function actionView($id, $m = null, $s = Message::SCROLL_UP)
    {
        $model            = new Message;
        $model->id        = $m;
        $model->dialog_id = $id;
        $model->user_id   = Yii::$app->user->id;
        $model->scroll    = $s;

        $dataProvider = $model->search();

        return $this->asJson([
            'success' => true,
            'count'   => $dataProvider->totalCount,
            'data'    => [
                'html'   => $this->renderPartial('partial/view', ['dataProvider' => $dataProvider]),
                'scroll' => $s,
            ],
        ]);
    }

    /**
     * Отправить сообщение для диалога
     *
     * @param integer $id
     */
    public function actionSend($id)
    {
        $model         = new Sender;
        $model->from   = Yii::$app->user->id;
        $model->to     = $id;
        $model->target = Sender::TARGET_DIALOG;

        if ($model->load(Yii::$app->request->post()) && $model->send()) {
            return $this->asJson([
                'success' => true,
                'data'    => [
                    'url' => Url::to(['view', 'id' => $id]),
                ]
            ]);
        } elseif ($model->hasErrors()) {
            $attribute = array_keys($model->errors)[0];

            return $this->asJson(['success' => false, 'data' => ['message' => $model->getFirstError($attribute)]]);
        }
    }

    /**
     * связаться с технической поддрежкой
     */
    public function actionContactSupport()
    {
        // идентификатор техподдержки
        $supportId = YII_ENV_DEV ? 4 : 154;
        $dialog    = Dialog::createForUsers([Yii::$app->user->id, $supportId]);

        return $this->redirect('/communication/dialog/index?id=' . $dialog->id);
    }

    /**
     * Скрыть диалог от пользователя
     *
     * @param integer $id
     */
    public function actionHide($id)
    {
        DialogLink::updateAll(['active' => false], [
            'dialog_id' => $id,
            'user_id'   => Yii::$app->user->id,
            'active'    => true,
        ]);

        return $this->redirect(['index']);
    }

}
