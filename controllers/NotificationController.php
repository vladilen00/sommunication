<?php

namespace app\modules\communication\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use app\modules\user\models\Profile;
use app\modules\communication\models\Notification;
use app\modules\communication\models\NotificationLink;

class NotificationController extends \app\components\AccountController {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		$behaviors = parent::behaviors();
		$behaviors['access'] = [
			'class' => 'yii\filters\AccessControl',
			'rules' => [
				[
					'allow' => true,
					'roles' => ['@'],
				]
			],
		];
		$behaviors['verbs'] = [
			'class' => 'yii\filters\VerbFilter',
			'actions' => [
				'delete' => ['post']
			]
		];
		return $behaviors;
	}

	/**
	 * Список уведомлений
	 * @param string $type
	 */
	public function actionIndex($type = Notification::TYPE_NEWS) {
		$model = new NotificationLink;
		$model->user_id = Yii::$app->user->id;
		$model->type = $type;
		$dataProvider = $model->search();
		$dataProvider->prepare();

		$new = array_column(array_filter($dataProvider->models, function ($model) {
			return !$model->viewed;
		}), 'notification_id');

		if (!empty($new)) {
			$model->updateAll(['viewed' => true], [
				'notification_id' => $new,
				'user_id' => $model->user_id,
			]);
		}
		return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider]);
	}

	/**
	 * Список последних уведомлений для dropdown
	 */
	public function actionLast() {
		$model = new NotificationLink;
		$model->user_id = Yii::$app->user->id;
		$model->type = 'all';
		$query = $model->search()->query;
		$query->limit(8);
		return $this->renderPartial('partial/last', ['models' => $query->all()]);
	}

	/**
	 * Получить кол-во непрочитанных уведомлений
	 */
	public function actionGetCount() {
		Yii::$app->response->format = 'json';
		return ['count' => NotificationLink::find()->andWhere(['user_id' => Yii::$app->user->id, 'viewed' => false])->count()];
	}

	/**
	 * Удалить уведомление
	 */
	public function actionDelete() {
		return $this->asJson([
			'success' => NotificationLink::deleteAll([
				'notification_id' => Yii::$app->request->get('id'),
				'user_id' => Yii::$app->user->id,
			]) > 0
		]);
	}

	public function actionUnsubscribe()
    {
        $model = Profile::findOne(['user_id' => Yii::$app->user->id]);
        $model->email_notify = 0;
        $model->save(false);
        $this->layout = '//home';
        return $this->render('unsubscribe');
    }
}
