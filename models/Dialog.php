<?php

namespace app\modules\communication\models;

use Yii;
use yii\helpers\Url;
use yii\db\IntegrityException;
use app\behaviors\DateTimeBehavior;
use app\modules\user\models\User;

/**
 * This is the model class for table "comm_dialog".
 *
 * @property integer      $id
 * @property string       $type
 * @property string       $created_at
 *
 * @property string       $viewUrl
 *
 * @property User[]       $users
 * @property DialogLink[] $links
 * @property Message[]    $messages
 * @property Message      $lastMessage
 */
class Dialog extends \yii\db\ActiveRecord
{

    const TYPE_DIALOG = '1';
    const TYPE_ROOM = '2';

    /**
     * @var integer
     */
    protected $last_message_id;

    /**
     * @var integer
     */
    public $new_message_count = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comm_dialog';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new class(get_called_class()) extends \yii\db\ActiveQuery
        {

            /**
             * @return $this
             */
            public function active()
            {
                return $this->innerJoin('comm_dialog_link', [
                    'and',
                    '`comm_dialog_link`.`dialog_id` = `comm_dialog`.`id`',
                    '`comm_dialog_link`.`user_id` = :uid',
                    ['comm_dialog_link.active' => true],
                ]);
            }

            /**
             * @return $this
             */
            public function sortByLastMessage($whitStatus = false)
            {
                $this->active();

                if ($this->select === null) {
                    $this->select('comm_dialog.*');
                }
                $this->addSelect(['last_message_id' => 'MAX(`comm_dialog_message`.`id`)']);
                $this->leftJoin('comm_dialog_message', [
                    'and',
                    '`comm_dialog_message`.`dialog_id` = `comm_dialog`.`id`',
                    '`comm_dialog_message`.`created_at` >= `comm_dialog_link`.`joined_at`',
                    '`comm_dialog_message`.`id` NOT IN (SELECT `message_id` FROM `comm_dialog_message_hidden` WHERE `user_id` = :uid)',
                ]);

                if ($whitStatus) {
                    $this->leftJoin('comm_dialog_message_new', '`comm_dialog_message_new`.`message_id` = `comm_dialog_message`.`id` AND `comm_dialog_message_new`.`user_id` = :uid');
                    $this->addSelect(['new_message_count' => 'SUM(IF(`comm_dialog_message_new`.`message_id` IS NULL, 0, 1))']);
                }
                $this->orderBy(['last_message_id' => SORT_DESC]);

                return $this;
            }
        };
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'datetime' => [
                'class'      => DateTimeBehavior::className(),
                'type'       => DateTimeBehavior::TYPE_DATETIME,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinks()
    {
        return $this->hasMany(DialogLink::className(), ['dialog_id' => 'id'])->inverseOf('dialog');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers($callback = null)
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->via('links', $callback);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['dialog_id' => 'id'])->inverseOf('dialog');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastMessage()
    {
        return $this->hasOne(Message::className(), ['dialog_id' => 'id', 'id' => 'last_message_id'])->inverseOf('dialog');
    }

    /**
     * @return string
     */
    public function getViewUrl()
    {
        return Url::to(['/communication/dialog/view', 'id' => $this->id]);
    }

    /**
     * @return \self
     */
    public static function create($type = self::TYPE_DIALOG)
    {
        $model       = new self;
        $model->type = $type;
        $model->save();

        return $model;
    }

    /**
     * создать диалог для двух пользователей, если его еще нет
     *
     * @param array  $ids  - идентификаторы пользователей
     * @param string $type - тип диалога
     *
     * @throws \Exception
     *
     * @return Dialog
     */
    public static function createForUsers($ids, $type = self::TYPE_DIALOG)
    {
        if (count($ids) != 2) {
            throw new \Exception('Для создания диалога необходимо два пользователя!');
        }

        if ($dialog = self::findDialogByUserIds($ids)) {
            return $dialog;
        }

        $dialog       = new self;
        $dialog->type = $type;
        $dialog->save();

        foreach ($ids as $id) {
            $dialog->addUser($id);
        }

        return $dialog;
    }

    /**
     * найти диалог по идентифкаторам пользователей
     *
     * @param $ids - идентификаторы пользователей, входящих в диалог
     *
     * @return Dialog|null
     */
    private static function findDialogByUserIds($ids)
    {
        $query = Dialog::find()->where(['type' => Dialog::TYPE_DIALOG]);
        $query->innerJoin(['l' => DialogLink::tableName()], [
            'and',
            '`l`.`dialog_id` = `comm_dialog`.`id`',
            ['l.user_id' => current($ids)],
            ['l.active' => true],
        ]);
        $query->innerJoin(['r' => DialogLink::tableName()], [
            'and',
            '`r`.`dialog_id` = `l`.`dialog_id`',
            ['r.user_id' => next($ids)],
            ['r.active' => true],
        ]);

        return $query->one();
    }

    public function addUser($users = null, $dt = null)
    {
        if ($users === null)
            $users = Yii::$app->user->id;

        if (!is_array($users))
            $users = (array)$users;

        if ($dt === null)
            $dt = date('Y-m-d H:i:s');

        $rows = array_map(function ($value) use ($dt) {
            return [$this->id, $value, true, $dt];
        }, $users);

        try {
            $this->getDb()->createCommand()->batchInsert(DialogLink::tableName(), ['dialog_id', 'user_id', 'active', 'joined_at'], $rows)->execute();
        } catch (IntegrityException $ex) {
            return false;
        }
    }

}
