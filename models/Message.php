<?php

namespace app\modules\communication\models;

use Yii;
use yii\db\IntegrityException;
use yii\data\ActiveDataProvider;
use app\behaviors\DateTimeBehavior;
use app\modules\user\models\User;
use app\helpers\StringHelper;
use app\data\ReversePagination;

/**
 * This is the model class for table "comm_dialog_message".
 *
 * @property string $id
 * @property integer $user_id
 * @property integer $dialog_id
 * @property string $text
 * @property string $created_at
 *
 * @property Dialog $dialog
 * @property User $user
 * 
 * @property boolean $isMy
 */
class Message extends \yii\db\ActiveRecord {

	const SCROLL_UP = -1;
	const SCROLL_INIT = 0;
	const SCROLL_DOWN = 1;

	public $unread;
	public $scroll;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'comm_dialog_message';
	}

	/**
	 * @inheritdoc
	 */
	public static function find() {
		return new class(get_called_class()) extends \yii\db\ActiveQuery {

			/**
			 * Добавить статус о прочтении сообщения пользователем в свойство `unread`
			 * @see addParams() - Нужно добавить параметр uid - id пользователя
			 * @return $this
			 */
			public function withStatus() {
				if ($this->select === null) {
					$this->select('comm_dialog_message.*');
				}
				$this->addSelect(['unread' => 'IF(`comm_dialog_message_new`.`message_id` IS NULL, FALSE, TRUE)']);
				$this->leftJoin('comm_dialog_message_new', [
					'and',
					'`comm_dialog_message_new`.`message_id` = `comm_dialog_message`.`id`',
					'`comm_dialog_message_new`.`user_id` = :uid',
				]);
				return $this;
			}

			/**
			 * Выбрать только непрочитанные для пользователя сообщения
			 * @see withStatus()
			 * @see addParams() - Нужно добавить параметр uid - id пользователя
			 * @return $this
			 */
			public function onlyNew() {
				return $this->withStatus()->andWhere(['is not', 'comm_dialog_message_new.message_id', null]);
			}

			/**
			 * Выбрать НЕ скрытые сообщения для пользователя
			 * @see addParams() - Нужно добавить параметр uid - id пользователя
			 * @return $this
			 */
			public function notHidden() {
				$this->leftJoin('comm_dialog_message_hidden', [
					'and',
					'`comm_dialog_message_hidden`.`message_id` = `comm_dialog_message`.`id`',
					'`comm_dialog_message_hidden`.`user_id` = :uid',
				]);
				$this->andWhere(['`comm_dialog_message_hidden`.`message_id`' => null]);
				return $this;
			}

			/**
			 * Выбрать все сообщения, которые доступны пользователю (из всех активных диалогов)
			 * @see addParams() - Нужно добавить параметр uid - id пользователя
			 * @return $this
			 */
			public function fromActiveDialogs() {
				$this->innerJoin('comm_dialog_link', [
					'and',
					'`comm_dialog_link`.`dialog_id` = `comm_dialog_message`.`dialog_id`',
					'`comm_dialog_link`.`joined_at` <= `comm_dialog_message`.`created_at`',
					'`comm_dialog_link`.`user_id` = :uid',
					['comm_dialog_link.active' => true],
				]);
				return $this;
			}

			/**
			 * Выбрать сообщения из диалога
			 * @param integer $id - id далога
			 * @see fromActiveDialogs()
			 * @see addParams() - Нужно добавить параметр uid - id пользователя
			 * @return $this
			 */
			public function fromDialog($id) {
				return $this->fromActiveDialogs()->andWhere(['comm_dialog_message.dialog_id' => $id]);
			}
		};
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'datetime' => [
				'class' => DateTimeBehavior::className(),
				'type' => DateTimeBehavior::TYPE_DATETIME,
				'attributes' => [
					self::EVENT_BEFORE_INSERT => ['created_at'],
				],
			]
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDialog() {
		return $this->hasOne(Dialog::className(), ['id' => 'dialog_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @return string
	 */
	public function getText($length = 0) {
		$text = $this->text;

		if ($length > 0)
			$text = StringHelper::substr($text, $length, '...');

		$text = preg_replace('#\bhttps?://[^\s<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#', '<a href="\\0" target="_blank">\\0</a>', $text);

		return '<p>' . preg_replace('/\n|\r\n?/u', '</p><p>', $text) . '</p>';
	}

	public function unread($for = null) {
		if ($for === null)
			$for = Yii::$app->user->id;

		if (!is_array($for))
			$for = (array) $for;

		$rows = array_map(function ($value) {
			return [$this->id, $value];
		}, $for);

		try {
			$this->getDb()->createCommand()->batchInsert('comm_dialog_message_new', ['message_id', 'user_id'], $rows)->execute();
		} catch (IntegrityException $ex) {
			return false;
		}
	}

	public function read($for = null) {
		if ($for === null)
			$for = Yii::$app->user->id;

		if (!is_array($for))
			$for = (array) $for;

		$this->getDb()->createCommand()->delete('comm_dialog_message_new', ['message_id' => $this->id, 'user_id' => $for])->execute();
		return true;
	}

	/**
	 * @param self $models
	 * @param integer|integer[] $for
	 */
	public static function readAll($models, $for = null) {
		foreach ($models as $model) {
			if ($model->unread) {
				$model->read($for);
			}
		}
	}

	/**
	 * @param self $models
	 * @param integer|integer[] $for
	 */
	public static function unreadAll($models, $for = null) {
		foreach ($models as $model) {
			$model->unread($for);
		}
	}

	/**
	 * @return boolean
	 */
	public function getIsMy() {
		return $this->user_id == Yii::$app->user->id;
	}

	/**
	 * @return ActiveDataProvider
	 */
	public function search($read = true) {
		$query = self::find();
		$query->addParams(['uid' => $this->user_id]);
		$query->with('user.profile.file');
		$query->fromDialog($this->dialog_id)->withStatus()->notHidden();

		$dataProvider = new ActiveDataProvider;

		switch ($this->scroll) {
			case self::SCROLL_UP:
				$query->andFilterWhere(['<', 'comm_dialog_message.id', $this->id]);
				$dataProvider->pagination = new ReversePagination(['pageSize' => 10]);
				break;
			case self::SCROLL_INIT:
				$query->andFilterWhere(['>=', 'comm_dialog_message.id', $this->id]);
				$dataProvider->pagination = false;
				break;
			case self::SCROLL_DOWN:
				$query->andFilterWhere(['>', 'comm_dialog_message.id', $this->id]);
				$dataProvider->pagination = false;
				break;
			default:
				$query->andWhere('0=1');
		}
		$dataProvider->query = $query;

		if ($read) {
			$dataProvider->prepare();
			self::readAll($dataProvider->models);
		}
		return $dataProvider;
	}

}
