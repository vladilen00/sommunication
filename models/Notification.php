<?php

namespace app\modules\communication\models;

use Yii;
use yii\db\IntegrityException;
use yii\base\ViewNotFoundException;
use app\helpers\StringHelper;
use app\behaviors\DateTimeBehavior;
use app\modules\user\models\User;
use app\modules\distribution\models\Queue;

/**
 * @property integer $id
 * @property string $action
 * @property integer $user_id
 * @property string $created_at
 * @property array|string $data
 * 
 * @property-read User $user
 * @property-read NotificationLink[] $links
 * 
 * @property-read string $type
 */
class Notification extends \yii\db\ActiveRecord {

	const TYPE_NEWS = 'news';
	const TYPE_SOCIAL = 'social';
	const TYPE_REPLY = 'reply';
	const TYPE_SYSTEM = 'system';
	const TYPE_OTHER = null;
	# SOCIAL
	const ACTION_SOCIAL_AREA_ASSIGN = 'social.area.assign';
	const ACTION_SOCIAL_AREA_CONFIRM = 'social.area.confirm';
	const ACTION_SOCIAL_TEST_ROOM_INVITE = 'social.test.room.invite';
	const ACTION_SOCIAL_TEST_ROOM_RESULT = 'social.test.room.result';
	# REPLY
	const ACTION_REPLY_THREAD_REPLY = 'reply.thread.reply';
	const ACTION_REPLY_THREAD_TOPICSTARTER = 'reply.thread.topicstarter';
	# SYSTEM
	const ACTION_SYSTEM_FEEDBACK_NEW = 'system.feedback.new';
	const ACTION_SYSTEM_SESSION_ASSIGN = 'system.session.assign';
	const ACTION_SYSTEM_SESSION_CANCEL_BY_CLIENT = 'system.session.cancel.byclient';
	const ACTION_SYSTEM_SESSION_CANCEL_BY_EXPERT = 'system.session.cancel.byexpert';
	const ACTION_SYSTEM_SESSION_MOVE = 'system.session.move';
	# NEWS
	const ACTION_NEWS_NEW = 'news.new';

	/**
	 * @var array
	 */
	public static $types = [
		self::TYPE_NEWS => 'assignment',
		self::TYPE_SOCIAL => 'accounts',
		self::TYPE_REPLY => 'mail-reply-all',
		self::TYPE_SYSTEM => 'notifications',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'comm_notification';
	}

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'datetime' => [
				'class' => DateTimeBehavior::className(),
				'type' => DateTimeBehavior::TYPE_DATETIME,
				'attributes' => [
					self::EVENT_BEFORE_INSERT => ['created_at'],
				]
			],
			'json' => [
				'class' => 'app\behaviors\JsonAttributeBehavior',
				'attributes' => ['data'],
				'asArray' => false,
				'options' => JSON_UNESCAPED_UNICODE
			]
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLinks() {
		return $this->hasMany(NotificationLink::className(), ['notification_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @return string
	 */
	public function getType() {
		return StringHelper::match('/^(\w+)\./', $this->action, 1);
	}

	/**
	 * @return string
	 */
	public function renderText($min = false) {
		$viewFile = str_replace('.', '-', $this->action);

		try {
			return Yii::$app->view->render("@app/modules/communication/views/notification/layouts/$viewFile", ['min' => $min, 'model' => $this]);
		} catch (ViewNotFoundException $ex) {
			return null;
		}
	}

	/**
	 * @param integer|array $users
	 * @return integer
	 */
	public function sendTo($users) {
		if ($this->isNewRecord && !$this->save() || empty($users))
			return false;

		if (!is_array($users))
			$users = (array) $users;

		$rows = array_map(function ($user) {
			return [
				'notification_id' => $this->id,
				'user_id' => $user,
			];
		}, $users);

		try {
			self::getDb()->createCommand()->batchInsert(NotificationLink::tableName(), ['notification_id', 'user_id'], $rows)->execute();

		} catch (IntegrityException $ex) {
			return false;
		}

        return Yii::$app->sendPulse->send($users, $this->getMessageTemplate());

	}

	/**
	 * @return boolean|integer
	 */
	public function refreshLinks() {
		$this->created_at = date('Y-m-d H:i:s');

		if ($this->save())
			return NotificationLink::updateAll(['viewed' => false], ['notification_id' => $this->id]);

		return false;
	}
	
	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getDataLangAttribute($name) {
		if (isset($this->data->$name)) {
			return $this->data->$name;
		} elseif (Yii::$app->language == 'ru-RU') {
			return $this->data->{$name . '_ru'};
		} else {
			return $this->data->{$name . '_en'};
		}
	}

	public function getMessageTemplate()
    {
        # SOCIAL | AREA ASSIGN
        # -------------------------------------------------
        if($this->action === self::ACTION_SOCIAL_AREA_ASSIGN) {
            return [
                'theme' => 'Запрос на добавление в круги ',
                'body'  => Yii::$app->view->renderFile('@app/modules/communication/views/email/assign.php', [
                    'model' => $this,
                ]),
            ];
        }

        # SOCIAL | ROOM INVITE
        # -------------------------------------------------
        if($this->action === self::ACTION_SOCIAL_TEST_ROOM_INVITE) {
            return [
                'theme' => 'Приглашение пройти тест',
                'body'  => Yii::$app->view->renderFile('@app/modules/communication/views/email/invite.php', [
                    'model' => $this,
                ]),
            ];
        }

        # NEWS | NEW
        # -------------------------------------------------
        if($this->action === self::ACTION_NEWS_NEW) {
            return [
                'theme' => $this->data->title_ru,
                'body'  => Yii::$app->view->renderFile('@app/modules/communication/views/email/news.php', [
                    'data' => $this->data
                ]),
            ];
        }

        # REPLY
        # -------------------------------------------------
        if($this->action === self::ACTION_REPLY_THREAD_REPLY) {
            return [
                'theme' => 'Ответ на Ваш комментарий',
                'body'  => Yii::$app->view->renderFile('@app/modules/communication/views/email/reply.php', [
                    'model' => $this
                ]),
            ];
        }

        return null;
    }
}
