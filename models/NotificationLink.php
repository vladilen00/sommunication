<?php

namespace app\modules\communication\models;

use Yii;
use app\modules\user\models\User;

/**
 * @property integer $notification_id
 * @property integer $user_id
 * @property boolean $viewed
 * 
 * @property Notification $notification
 * @property User $user
 */
class NotificationLink extends \yii\db\ActiveRecord {

	/**
	 * @var string
	 */
	public $type;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'comm_notification_link';
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getNotification() {
		return $this->hasOne(Notification::className(), ['id' => 'notification_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @return \yii\data\ActiveDataProvider
	 */
	public function search() {
		$query = self::find();
		$query->joinWith('notification', true, 'INNER JOIN');
		$query->with('notification.user.profile.file');
		$query->andWhere(['comm_notification_link.user_id' => $this->user_id]);

		if ($this->type !== 'all') {
			$query->andWhere(['like', 'comm_notification.action', "$this->type.%", false]);
		}
		$query->orderBy(['comm_notification.created_at' => SORT_DESC]);
		
		return new \yii\data\ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 15
			]
		]);
	}
}
