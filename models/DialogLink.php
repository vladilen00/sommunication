<?php

namespace app\modules\communication\models;

use app\modules\user\models\User;

/**
 * @property integer $id
 * @property integer $dialog_id
 * @property integer $user_id
 * @property boolean $active
 * @property string $joined_at
 * 
 * @property-read Dialog $dialog
 * @property-read User $user
 */
class DialogLink extends \yii\db\ActiveRecord {
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'comm_dialog_link';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function find() {
		return new class(get_called_class()) extends \yii\db\ActiveQuery {
			
			public function active() {
				return $this->andOnCondition(['comm_dialog_link.active' => true]);
			}
		};
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDialog() {
		return $this->hasOne(Dialog::className(), ['id' => 'dialog_id']);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
