<?php

namespace app\modules\communication\models;

use Yii;
use yii\db\Query;
use app\validators\TextFilter;
use app\modules\user\models\User;

/**
 * @property-read Dialog $dialog
 * @property-read Message $message
 */
final class Sender extends \yii\base\Model {

	const TARGET_DIALOG = 'dialog';
	const TARGET_USER = 'user';

	public $from;
	public $to;
	public $target;
	public $text;

	/**
	 * @var Dialog
	 */
	private $_dialog;

	/**
	 * @var Message
	 */
	private $_message;

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		return [
			self::SCENARIO_DEFAULT => ['text', '!to'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[['to', 'text'], 'required'],
			['text', TextFilter::className()],
			['text', 'string', 'max' => 1000],
			[
				'to',
				'compare',
				'compareAttribute' => 'from',
				'operator' => '!=',
				'message' => Yii::t('app/communication', 'models.Sender.text-1'),
				'when' => function () {
					return $this->target == self::TARGET_USER;
				},
			],
			[
				'to',
				'exist',
				'targetClass' => User::className(),
				'targetAttribute' => 'id',
				'message' => Yii::t('app/communication', 'models.Sender.text-2'),
				'filter' => function ($query) {
					$query->notInBlacklist();
				},
				'when' => function () {
					return $this->target == self::TARGET_USER;
				},
			],
			[
				'to',
				'exist',
				'targetClass' => Dialog::className(),
				'targetAttribute' => 'comm_dialog.id',
				'filter' => function ($query) {
					$query->addParams(['uid' => $this->from]);
					$query->active();
				},
				'message' => Yii::t('app/communication', 'models.Sender.text-3'),
				'when' => function () {
					return $this->target == self::TARGET_DIALOG;
				},
			],
		];
	}

	/**
	 * @return Dialog
	 */
	public function getDialog() {
		return $this->_dialog;
	}

	/**
	 * @return Message
	 */
	public function getMessage() {
		return $this->_message;
	}

	/**
	 * @return boolean
	 */
	public function send() {
		if (!$this->validate())
			return false;

		if ($this->target == self::TARGET_USER) {
			$this->_dialog = $this->findDialog();

			if ($this->_dialog === null) {
				$this->_dialog = Dialog::create();
				$this->_dialog->addUser($this->from);
				$this->_dialog->addUser($this->to);
			} else {
				if (!DialogLink::find()->andWhere(['dialog_id' => $this->_dialog->id, 'user_id' => $this->from])->active()->exists())
					$this->_dialog->addUser($this->from);

				if (!DialogLink::find()->andWhere(['dialog_id' => $this->_dialog->id, 'user_id' => $this->to])->active()->exists())
					$this->_dialog->addUser($this->to);
			}
		} else {
			$this->_dialog = Dialog::findOne($this->to);

			if ($this->_dialog->type == Dialog::TYPE_DIALOG && !$this->checkDialogBlacklist()) {
				$this->addError('to', Yii::t('app/communication', 'models.Sender.text-2'));
				return false;
			}
		}
		$this->_message = new Message(['user_id' => $this->from, 'dialog_id' => $this->_dialog->id, 'text' => $this->text]);
		$this->_message->save();

		if ($this->target == self::TARGET_USER) {
			$this->_message->unread($this->to);
		} else {
			$query = $this->_dialog->getLinks();
			$query->select(['user_id', 'active' => 'SUM(`active`)']);
			$query->andWhere(['!=', 'user_id', $this->from]);
			$query->groupBy('user_id');
			$users = $query->createCommand()->queryAll();

			$add = array_filter($users, function ($item) {
				return $item['active'] == 0;
			});
			$add = array_column($add, 'user_id');

			if (!empty($add))
				$this->_dialog->addUser($add, $this->_message->created_at);

			$unread = array_column($users, 'user_id');
			$this->_message->unread($unread);
		}
		return true;
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'text' => Yii::t('app/communication', 'views.dialog.partial.enter.text-9'),
			'to' => Yii::t('app/communication', 'models.Sender.text-5'),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeHints() {
		return [
			'text' => Yii::t('app/communication', 'models.Sender.text-4')
		];
	}

	/**
	 * @return Dialog
	 */
	private function findDialog() {
		$query = Dialog::find();
		$query->innerJoin(['l' => DialogLink::tableName()], [
			'and',
			'`l`.`dialog_id` = `comm_dialog`.`id`',
			['l.user_id' => $this->from],
			['l.active' => true],
		]);
		$query->innerJoin(['r' => DialogLink::tableName()], [
			'and',
			'`r`.`dialog_id` = `l`.`dialog_id`',
			['r.user_id' => $this->to],
			['r.active' => true],
		]);
		$query->andWhere(['comm_dialog.type' => Dialog::TYPE_DIALOG]);
		return $query->one();
	}

	/**
	 * @return boolean
	 */
	private function checkDialogBlacklist() {
		$query = new Query;
		$query->from(['b' => 'user_blacklist']);
		$query->leftJoin(['l' => 'comm_dialog_link'], '`l`.`user_id` = `b`.`from_user_id` AND `l`.`active` = TRUE');
		$query->andWhere([
			'l.dialog_id' => $this->_dialog->id,
			'b.to_user_id' => $this->from,
		]);
		return !$query->exists();
	}

}
