<?php

namespace app\modules\communication\models;

use Yii;
use app\modules\user\models\User;

/**
 * @property integer $user_id
 * @property string $dt_notify
 * @property integer $count
 * 
 * @property User $user
 */
class MessageEmail extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'comm_dialog_message_email';
	}
	
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			[
				'class' => 'app\behaviors\DateTimeBehavior',
				'type' => 'datetime',
				'attributes' => [
					self::EVENT_BEFORE_INSERT => ['dt_notify'],
					self::EVENT_BEFORE_UPDATE => ['dt_notify'],
				]
			]
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	/**
	 * @param User $user
	 * @return self
	 */
	public static function send($user) {
		$model = self::findOne($user->id);

		if ($model === null) {
			$model = new self;
			$model->user_id = $user->id;
			$model->count = 0;
		}
		$result = Yii::$app->mailer->compose('@app/mail/message-email', [
			'username' => $user->profile->name,
			'dialogCount' => $user->dialog_count,
			'messageCount' => $user->dialog_message_count,
			'firstMessageDate' => date('d.m.Y H:i', $user->dialog_first_message_dt),
		])
		->setFrom(Yii::$app->params['email'])
		->setTo($user->email)
		->setSubject(Yii::$app->name . ' - ' . Yii::t('app/communication', 'models.MessageEmail.text-1'))
		->send();

		if ($result) {
			$model->count++;
			$model->save();
		}
		return $model;
	}

}
